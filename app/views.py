from django.shortcuts import render
from django.utils import timezone


def my_view(request):
	contexto = {
		"usuario": "alumno",
		"amigos": 99,
		"administrador": False,
		"cursos": ["django", "nodejs", "javaescrip"],
		"cadena_vacia": "",
		"fecha_actual": timezone.now()
	}
	return render(request, "my_template.html", contexto)