from django.db import models

class Person(models.Model):
	fist_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=30)
	home_address = models.TextField(null = True, blank=True)
	birthday = models.DateField()
	photo = models.ImageField(upload_to="photos_media")

