from django.contrib import admin
from .models import Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    fields = ['fist_name','last_name', 'home_address', 'birthday', 'photo']