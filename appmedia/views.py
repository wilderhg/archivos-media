from django.shortcuts import render
from django.utils import timezone
from .models import Person

def my_view(request):
	person = Person.objects.get(id=1)
	contexto = {
		"person": person,
	}
	
	return render(request, "media.html", contexto)