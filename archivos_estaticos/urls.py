from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'archivos_estaticos.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^estaticos/','app.views.my_view'),
    url(r'^appmedia/', 'appmedia.views.my_view'),
    #incluir a urls
    #urls(r'^', include('appmedia.urls', namespace="appmedia")),
)


if settings.DEBUG:
	urlpatterns += patterns('',
 (r'^media/(?P<path>.*)$', 'django.views.static.serve',
{'document_root': settings.MEDIA_ROOT}),)